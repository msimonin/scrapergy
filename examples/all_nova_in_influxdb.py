from influxdb import DataFrameClient
import pandas as pd
from scrapergy import *


# create the influxdb client
client = DataFrameClient(host='127.0.0.1', port=8086)

# getting all the metrics
clusters = get_all_clusters_obj()
nova = [c for c in clusters if c.uid == "nova"][0]
for node in nova.nodes.list():
    data = download_metrics(node.uid,
                            "2019-05-27T13",
                            skip_header=1,
                            skip_footer=1)
    # Let's convert this as a pandas.DataFrame
    # Timestamps need to be converted to int representing ns
    df = pd.DataFrame(data=data[:,1],
                      index=(data[:,0]*10**9),
                      columns=['power'])
    df.index = df.index.astype(int)
    df.index = pd.to_datetime(df.index)
    # Ingest the data to influxdb, setting the host as a tag
    # This assumes the database "mymetrics" exists.
    client.write_points(df,
                        "power",
                        database="mymetrics",
                        tags={"node": node.uid})
