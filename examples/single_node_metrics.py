from scrapergy import *

# the resulting data is a numpy array of 1 hour power consumption data
# kwargs are passed to numpy.genfromtxt function
data = download_metrics("nova-2", "2019-05-28T14", skip_header=1, skip_footer=1)

import matplotlib.pyplot as plt

plt.plot(data[:, 0], data[:, 1])
plt.show()
