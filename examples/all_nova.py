from scrapergy import *
import matplotlib.pyplot as plt

clusters = get_all_clusters_obj()
nova = [c for c in clusters if c.uid == "nova"][0]
for node in nova.nodes.list():
    data = download_metrics(node.uid,
                            "2019-05-27T13", 
                            skip_header=1, 
                            skip_footer=1)
    plt.plot(data[:, 0], data[:, 1])

plt.show()
