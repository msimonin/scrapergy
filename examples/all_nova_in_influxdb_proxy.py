import os

from influxdb import DataFrameClient
import pandas as pd
from scrapergy import *


def save_http_proxys():
    return (os.environ.get("http_proxy"),
            os.environ.get("https_proxy"))


def set_http_proxys(proxys):
    http, https = proxys
    os.environ["http_proxy"] = http
    os.environ["https_proxy"] = https


def clear_http_proxys():
    if os.environ.get("http_proxy"):
        os.environ.pop("http_proxy")
    if os.environ.get("https_proxy"):
        os.environ.pop("https_proxy")


proxys = save_http_proxys()

# create the influxdb client
client = DataFrameClient(host='127.0.0.1', port=8086)

# getting all the metrics
clusters = get_all_clusters_obj()
nova = [c for c in clusters if c.uid == "nova"][0]
# in this order we benefit more from the cache
for hour in range(24):
    for node in nova.nodes.list():
        set_http_proxys(proxys)
        data = download_metrics(node.uid,
                                "2019-05-27T%02d" % hour,
                                skip_header=1,
                                skip_footer=1)
        # Let's convert this as a pandas.DataFrame
        # Timestamps need to be converted to int representing ns
        df = pd.DataFrame(data=data[:,1],
                        index=(data[:,0]*10**9),
                        columns=['power'])
        df.index = df.index.astype(int)
        df.index = pd.to_datetime(df.index)
        # Ingest the data to influxdb, setting the host as a tag
        # This assumes the database "mymetrics" exists.
        clear_http_proxys()
        client.write_points(df,
                            "power",
                            database="mymetrics",
                            tags={"node": node.uid})
