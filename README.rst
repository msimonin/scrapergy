=========
scrapergy
=========


``scrapergy`` is a small python package to get the energy metrics from your nodes
(esp. nodes located in Lyon). It was initialy developed as a toy project to test
the ring library (`https://ring-cache.readthedocs.io/en/stable/ <https://ring-cache.readthedocs.io/en/stable/>`_) in conjonction
with python-grid5000 (`https://msimonin.gitlabpages.inria.fr/python-grid5000 <https://msimonin.gitlabpages.inria.fr/python-grid5000>`_)

.. warning::

    The code is currently being developed heavily. Jump to the contributing section
    if you want to be involved.

1 Contributing
--------------

- To contribute, you can drop me an email or open an issue for a bug report, or feature request.

- Ideas:

  - Add debug messages

  - Passing a job id

  - Support for pandas dataframe instead of numpy

2 Installation and examples
---------------------------

::

    echo '
    username: MYLOGIN
    password: MYPASSWORD
    verify_ssl: False
    ' > ~/.python-grid5000.yaml

- Using a virtualenv is recommended (python 3.5+ is required)

::

    virtualenv -p python3 venv
    source venv/bin/activate
    pip install python-grid5000


Outside Grid’5000, you can set-up a proxy socks. Thre requests will be
automaticaly forwarded inside Grid’5000.


::

    # in a terminal
    ssh -ND 2100 access.grid5000.fr

    # on a another, where you'll test scrapergy
    export http_proxy=socksh://localhost:2100
    export https_proxy=socksh://localhost:2100

2.1 Get node information
~~~~~~~~~~~~~~~~~~~~~~~~

.. code:: python

    from scrapergy import *

    # the resulting data is a numpy array of 1 hour power consumption data
    # kwargs are passed to numpy.genfromtxt function
    data = download_metrics("nova-2", "2019-05-28T14", skip_header=1, skip_footer=1)

    import matplotlib.pyplot as plt

    plt.plot(data[:, 0], data[:, 1])
    plt.show()

2.2 All nova cluster
~~~~~~~~~~~~~~~~~~~~

The following will result in an unreadable plot. but illustrate the benefit of
caching the requests.

.. code:: python

    from scrapergy import *
    import matplotlib.pyplot as plt

    clusters = get_all_clusters_obj()
    nova = [c for c in clusters if c.uid == "nova"][0]
    for node in nova.nodes.list():
        data = download_metrics(node.uid,
                                "2019-05-27T13", 
                                skip_header=1, 
                                skip_footer=1)
        plt.plot(data[:, 0], data[:, 1])

    plt.show()

2.3 Ingest the data in influxdb, visualize with grafana
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The folowing will let you ingest the data in influxdb, explore them using grafana (neat!). 


::

    # Start influxdb and grafana
    docker run -p 8086:8086 -e "INFLUXDB_DB=mymetrics" --name influxdb -ti influxdb
    docker run -d --name=grafana --link influxdb:influxdb -p 3000:3000 --name grafana grafana/grafana

Once both containers are ready, add influxdb as a datasource in grafana. It’s
time to ingest some data in influxdb.

.. code:: python

    from influxdb import DataFrameClient
    import pandas as pd
    from scrapergy import *


    # create the influxdb client
    client = DataFrameClient(host='127.0.0.1', port=8086)

    # getting all the metrics
    clusters = get_all_clusters_obj()
    nova = [c for c in clusters if c.uid == "nova"][0]
    for node in nova.nodes.list():
        data = download_metrics(node.uid,
                                "2019-05-27T13",
                                skip_header=1,
                                skip_footer=1)
        # Let's convert this as a pandas.DataFrame
        # Timestamps need to be converted to int representing ns
        df = pd.DataFrame(data=data[:,1],
                          index=(data[:,0]*10**9),
                          columns=['power'])
        df.index = df.index.astype(int)
        df.index = pd.to_datetime(df.index)
        # Ingest the data to influxdb, setting the host as a tag
        # This assumes the database "mymetrics" exists.
        client.write_points(df,
                            "power",
                            database="mymetrics",
                            tags={"node": node.uid})

.. image:: ./exploration.png

2.4 Ingest data in influxdb and proxy handling
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Let’s ingest a whole day of data. Assuming influxdb is on running on
localhost, we need to clear the proxy socks before throwing metrics at him
(and restore it accordingy to the env var before scraping new metrics).

.. code:: python

    import os

    from influxdb import DataFrameClient
    import pandas as pd
    from scrapergy import *


    def save_http_proxys():
        return (os.environ.get("http_proxy"),
                os.environ.get("https_proxy"))


    def set_http_proxys(proxys):
        http, https = proxys
        os.environ["http_proxy"] = http
        os.environ["https_proxy"] = https


    def clear_http_proxys():
        if os.environ.get("http_proxy"):
            os.environ.pop("http_proxy")
        if os.environ.get("https_proxy"):
            os.environ.pop("https_proxy")


    proxys = save_http_proxys()

    # create the influxdb client
    client = DataFrameClient(host='127.0.0.1', port=8086)

    # getting all the metrics
    clusters = get_all_clusters_obj()
    nova = [c for c in clusters if c.uid == "nova"][0]
    # in this order we benefit more from the cache
    for hour in range(24):
        for node in nova.nodes.list():
            set_http_proxys(proxys)
            data = download_metrics(node.uid,
                                    "2019-05-27T%02d" % hour,
                                    skip_header=1,
                                    skip_footer=1)
            # Let's convert this as a pandas.DataFrame
            # Timestamps need to be converted to int representing ns
            df = pd.DataFrame(data=data[:,1],
                            index=(data[:,0]*10**9),
                            columns=['power'])
            df.index = df.index.astype(int)
            df.index = pd.to_datetime(df.index)
            # Ingest the data to influxdb, setting the host as a tag
            # This assumes the database "mymetrics" exists.
            clear_http_proxys()
            client.write_points(df,
                                "power",
                                database="mymetrics",
                                tags={"node": node.uid})
