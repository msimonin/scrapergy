import io
import gzip
import os
import numpy as np
import requests
import threading


import diskcache
from grid5000 import Grid5000
import ring


URL = "http://wattmetre.lyon.grid5000.fr/data"

_api_lock = threading.Lock()
# Keep track of the api client
_api_client = None

storage = diskcache.Cache('cachedir')

def get_api_client():
    """Gets the reference to the API cient (singleton)."""
    with _api_lock:
        global _api_client
        if not _api_client:
            conf_file = os.path.join(os.environ.get("HOME"),
                                     ".python-grid5000.yaml")
            _api_client = Grid5000.from_yaml(conf_file)

        return _api_client


@ring.disk(storage)
def get_sites_obj():
    """Get all the sites."""
    gk = get_api_client()
    return gk.sites.list()


@ring.disk(storage)
def get_all_clusters_obj():
    """Get all the clusters."""
    gk = get_api_client()
    sites = get_sites_obj()
    clusters = []
    for site in sites:
        # should we cache the list aswell ?
        clusters.extend(site.clusters.list())
    return clusters


@ring.disk(storage)
def get_node_obj(nodename):
    """Get a specific Node."""
    _cluster = nodename.split("-")[0]
    all_clusters = get_all_clusters_obj()
    cluster = [c for c in all_clusters if c.uid == _cluster]
    if len(cluster) != 1:
        raise ValueError
    return cluster[0].nodes[nodename]


@ring.disk(storage)
def get_wattmeter(nodename):
    """Get the wattmeter information"""
    node = get_node_obj(nodename)
    try:
        pdu = node.sensors["power"]["via"]["pdu"][0]
        return pdu["uid"], pdu["port"]
    except:
        ValueError
    

@ring.disk(storage)
def _download_metrics(url):
    # Beware that this load in memory the uncompressed file
    r = requests.get(url)
    f = gzip.decompress(r.content).decode()
    return f


def download_metrics(nodename, date, **kwargs):
    """Get a numpy array for the metrics, kwargs  is passed to the genfromtxt method"""
    pdu_uid, pdu_port = get_wattmeter(nodename)
    url = os.path.join(URL, 
                       "{pdu_uid}-log/power.csv.{date}.gz".format(
                           pdu_uid=pdu_uid, 
                           date=date))
    metrics = _download_metrics(url)
    data = io.StringIO(metrics)
    print(metrics[0:100])
    array = np.genfromtxt(data, delimiter=",", usecols=(2, pdu_port + 4), dtype=None, **kwargs)
    return array